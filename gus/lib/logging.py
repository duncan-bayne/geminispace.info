import os
import sys
import logging.config


def add_arguments(parser):
    """Add arguments to the given argument argparse parser."""

    parser.add_argument(
        "--logging-config",
        "-c",
        dest="logging_ini_fname",
        default=False,
        help="Location of logging configuration file",
    )


def handle_arguments(args):
    """Take arguments from argparse arguments and configure logging."""

    if args.logging_ini_fname:
        if os.path.isfile(args.logging_ini_fname):
            logging.config.fileConfig(args.logging_ini_fname)
        else:
            sys.exit("Can not find logging ini file: %s" % args.logging_ini_fname)

    elif os.path.isfile("logging.ini"):
        logging.config.fileConfig("logging.ini")


def strip_control_chars(s):
    if not s:
        return ""
    return "".join(i for i in s if 31 < ord(i) < 127)
