from gus import constants
from gus.lib.db_model import init_db, Link, Page
from gus.lib.gemini import GeminiResource, GeminiRobotFileParser

def main():
    db = init_db(f"index.new/{constants.DB_FILENAME}")
    PageFrom = Page.alias()
    PageTo = Page.alias()
    link_query = (Link
                   .select(Link, PageFrom, PageTo)
                   .join(PageFrom, on=(Link.from_page_id == PageFrom.id))
                   .join(PageTo, on=(Link.to_page_id == PageTo.id)))
    for link in link_query.iterator():
        from_resource = GeminiResource(link.from_page.fetchable_url)
        to_resource = GeminiResource(link.to_page.fetchable_url)
        is_cross_host_like = Link.get_is_cross_host_like(from_resource, to_resource)
        link.is_cross_host_like = is_cross_host_like
        link.save()
        print("[{}] {} -> {}".format("T" if is_cross_host_like else "F", from_resource.fetchable_url, to_resource.fetchable_url))
    print("\nDone!")


if __name__ == "__main__":
    main()
