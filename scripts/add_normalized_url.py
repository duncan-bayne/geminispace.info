from gus import constants
from gus.lib.db_model import init_db, Page
from gus.lib.gemini import GeminiResource, GeminiRobotFileParser

def main():
    db = init_db(f"index.new/{constants.DB_FILENAME}")
    for page in Page.select():
        print(f"\nBefore: {page.normalized_url}")
        page.normalized_url = GeminiResource(page.url).normalized_url
        page.save()
        print(f"After : {page.normalized_url}")
    print("\nDone!")


if __name__ == "__main__":
    main()
