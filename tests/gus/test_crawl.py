import pytest

from gus.crawl import should_skip
from gus.lib.gemini import GeminiResource

class TestUrlExclusion:
    @pytest.mark.parametrize("test_url,expected_result", [
        ("gemini://gemini.circumlunar.space/favicon.ico", True),
        ("gemini://gemini.circumlunar.space/rss.txt", True),
    ])
    def test_excluded_url_paths(self, test_url, expected_result):
        resource = GeminiResource(test_url)
        assert should_skip(resource) == expected_result


    @pytest.mark.parametrize("test_url,expected_result", [
        ("gemini://hannuhartikainen.fi/twinwiki/_revert/1594367314474", True),
        ("gemini://hannuhartikainen.fi/twinwiki/1594367314474", False),
        ("gemini://hannuhartikainen.fi/twinwiki/Sandbox/_history/1594037613712", True),
        ("gemini://hannuhartikainen.fi/twinwiki", False),
        ("gemini://123456.ch", True),
        ("gemini://123456.ch/fnord", True),
        ("gemini://almp1234.app", True),
        ("gemini://almp1234.app/fnord", True),
    ])
    def test_excluded_url_pattern(self, test_url, expected_result):
        resource = GeminiResource(test_url)
        assert should_skip(resource) == expected_result


    @pytest.mark.parametrize("test_url,expected_result", [
        ("gemini://localhost", True),
        ("gemini://example.org", True),
        ("gus.guru", False),
        ("gus.guru/search?turkey", True),
    ])
    def test_excluded_url_prefixes(self, test_url, expected_result):
        resource = GeminiResource(test_url)
        assert should_skip(resource) == expected_result
