{% include 'fragments/header.gmi' %}


## Backlinks for {{ url }}

{% if external_backlinks | length > 1 %}
### {{ external_backlinks|length }} cross-capsule backlinks

{% elif external_backlinks | length > 0 %}
### {{ external_backlinks|length }} cross-capsule backlink

{% else %}
### No cross-capsule backlinks

Instead, here's a duck:
```
         ,~~.
        (  6 )-_,
   (\___ )=='-'
    \ .   ) )
     \ `-' /
  ~`~'`~'`~'`~`~
```
{% endif %}
{% for backlink in external_backlinks %}
=> {{ backlink }} {{ backlink[9:] }}
{% endfor %}

{% if internal_backlinks | length > 1 %}
### {{ internal_backlinks|length }} internal backlinks

{% elif internal_backlinks | length > 0 %}
### {{ internal_backlinks|length }} internal backlink

{% else %}
### No internal backlinks

{% if external_backlinks | length > 0 %}
Instead, here's a duck:
{% else %}
Instead, here's a friend for the other duck:
{% endif %}
```
         ,~~.
        (  6 )-_,
   (\___ )=='-'
    \ .   ) )
     \ `-' /
  ~`~'`~'`~'`~`~
```
{% endif %}
{% for backlink in internal_backlinks %}
=> {{ backlink }} {{ backlink[9:] }}
{% endfor %}

{% include 'fragments/footer.gmi' %}
