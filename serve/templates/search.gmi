{% include 'fragments/header.gmi' %}


{% if verbose %}
## Search [verbose]
=> /search/{{ current_page }}?{{ quoted_query }} Exit verbose mode
{% else %}
## Search
=> /v/search/{{ current_page }}?{{ quoted_query }} Enter verbose mode
{% endif %}

"{{ query }}"

{% for result in results %}
=> {{ result["fetchable_url"] }} {{ result["link_text"] }}
{% if result["backlink_count"] > 1 %}
=> /backlinks?{{ result["url"][9:] | urlencode }} {{ result["backlink_count"] }} cross-capsule backlinks
{% elif result["backlink_count"] > 0 %}
=> /backlinks?{{ result["url"][9:] | urlencode }} {{ result["backlink_count"] }} cross-capsule backlink
{% endif %}
{% if verbose %}
* Score      : {{ "{:.2f}".format(result["score"]) }}
* Indexed at : {{ "{:%Y-%m-%d %H:%M}".format(result["indexed_at"]) }}
* Charset    : {{ result["charset"] }}
{% endif %}
{% if result["highlights"] | length > 0 %}
{{ result["highlights"] }}
{% endif %}

{% else %}
No results!

{% endfor %}
{% include 'fragments/pager.gmi' %}

{% include 'fragments/footer.gmi' %}
