import argparse

import jetforce

from . import app, gus


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--host", help="Server address to bind to", default="127.0.0.1")
    parser.add_argument("--port", help="Server port to bind to", type=int, default=1965)
    parser.add_argument("--hostname", help="Server hostname", default="localhost")
    parser.add_argument(
        "--tls-certfile",
        dest="certfile",
        help="Server TLS certificate file",
        metavar="FILE",
    )
    parser.add_argument(
        "--tls-keyfile",
        dest="keyfile",
        help="Server TLS private key file",
        metavar="FILE",
    )

    return parser.parse_args()


def main():
    args = parse_args()
    server = jetforce.GeminiServer(
        app=app,
        host=args.host,
        port=args.port,
        hostname=args.hostname,
        certfile=args.certfile,
        keyfile=args.keyfile,
    )
    try:
        server.run()
    finally:
        gus.index.close()


if __name__ == "__main__":
    main()
